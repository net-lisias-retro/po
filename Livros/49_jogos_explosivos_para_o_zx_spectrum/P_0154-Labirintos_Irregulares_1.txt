'- Labirintos Irregulares 1/3, ZX-Spectrum , 49 Jogos Explosivos Para o ZX Spectrum , Página 154
'- com erros
'- Digitador desconhecido

10 DIM a$(9999)
20 DIM b$(9999)
30 INPUT "Numero (2 a 23)? ";s
40 LET t=INT (255/s)*s
45 LET w=INT (175/s)*s
50 PLOT 0,0
60 DRAW w/2,w/2
62 DRAW -w/2,w/2
64 DRAW (t-w)/2,0
66 DRAW w/2-s,-w/2+s
68 DRAW w/2-s,w/2-s
70 DRAW (t-w)/2,0
72 DRAW -w/2,-w/2
74 DRAW w/2,-w/2
76 DRAW -(t-w)/2,0
78 DRAW -w/2+s,w/2-s
80 DRAW -w/2+s,-w/2+s
90 DRAW -(t-w)/2,0
100 LET x=s*12
110 LET y=s*10
120 LET z=1
130 PLOT x,y
140 IF (POINT (x+s,y)+POINT (x-s,y)+POINT (x,y+s)+POINT (x,y-s))<>4 THEN GO TO 200
150 LET z=z-1
160 IF z=0 THEN STOP
170 LET x=CODE a$(z)
180 LET y=CODE b$(z)
190 GO TO 130
200 LET a$(z)=CHR$ x
210 LET b$(z)=CHR$ y
220 LET z=z+1
230 LET r=INT (RND*4)
240 LET c=s*((r=0)-(r=1))
250 LET d=s*((r=2)-(r=3))
260 IF POINT (x+c,y+d) THEN GO TO 230
270 DRAW c,d
280 LET x=x+c
290 LET y=y+d
300 GO TO 130
