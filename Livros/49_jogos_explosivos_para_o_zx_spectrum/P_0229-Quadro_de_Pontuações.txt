'- Quadro de Pontuações , ZX-Spectrum , 49 Jogos Explosivos Para o ZX Spectrum , Página 229
'- não testado
'- Digitador desconhecido

1 REM QUADRO DE PONTUACOES
2 REM \* DAVID PERRY 1983
3 REM
4 REM Num=Numero de pontuacoes
5 REM
6 REM
9 BORDER 0: PAPER 0: INK 7: BRIGHT 1: CLS
10 LET num=10: DIM n(num+1): DIM n$(num+1,8): FOR n=1 to num: LET n(n)=1100-(n*100): LET n$(n)="SPECTRUM": NEXT n
15 CLS : INPUT "Para exemplo, indique pontuacao      Pontuacao: ";sc
20 IF sc<=n(10) THEN GO TO 200
30 LET num=11: IF sc>=n(num) THEN INPUT "8 iniciais! ";p$: IF LEN p$>8 THEN GO TO 20
35 PRINT AT 7,0; FLASH 1; BRIGHT 0; INK 7; PAPER 2;"          Um momento .          "
40 IF sc>=n(num) THEN LET n(num)=sc: LET n$(num)=p$
50 FOR a=1 TO (num-1): LET b$=n$(a): LET c$=n$(a+1): LET b=n(a): LET c=n(a+1): IF b<c THEN LET n(a)=c: LET n(a+1)=b: LET n$(a)=c$: LET n$(a+1)=b$
60 NEXT a: FOR n=1 TO num-1: IF n(n)<n(n+1) THEN GO TO 50
70 NEXT n
80 CLS
90 PRINT AT 2,6;"QUADRO DE PONTUACOES"
100 PRINT AT 3,6;"===================="
110 FOR n=1 to num-1: PRINT AT n+5,7; INK 6;"("; INK 2;n; INK 6;")";AT n+5,12; INK 7;n(n): PRINT AT n+5,17; INK 5;n$(n): NEXT n
120 PLOT 0,0: DRAW 255,0: DRAW 0,175: DRAW -255,0: DRAW 0,-175: PRINT AT 17,6; PAPER 2; INK 7;"Carregue numa tecla.": LET i=0
130 LET LET i=i+1: IF i>7 THEN LET i=0
140 PRINT AT 2,5; INK 1;"QUADRO DE PONTUACOES"
150 BEEP .01,i*7: PAUSE 2: IF INKEY$="" THEN GO TO 130
160 GO TO 15
200 PRINT AT 10,4;"Pontuacao muito baixa..."
210 PRINT AT 13,6;"Carregue numa tecla."
220 PAUSE 0: RUN
